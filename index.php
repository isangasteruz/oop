<?php

    require_once ('animal.php');
    require_once ('frog.php');
    require_once ('ape.php');

    $object = new Animal("shaun");

    echo "Nama : " . $object->name . "<br>";
    echo "Kaki : " . $object->legs . "<br>";
    echo "Cold Blooded : " . $object->cold_blooded . "<br><br>";

    $object2 = new Frog("buduk");

    echo "Nama : " . $object2->name . "<br>";
    echo "Kaki : " . $object2->legs . "<br>";
    $object2->jump();

    $object3 = new Ape("sungokong");

    echo "Nama : " . $object3->name . "<br>";
    echo "Kaki : " . $object3->legs . "<br>";
    $object3->yell();